<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comprobante
 *
 * @ORM\Table(name="comprobante")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\ComprobanteRepository")
 */
class Comprobante
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer")
     */
    private $numero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=255)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="concepto", type="string", length=255)
     */
    private $concepto;

    /**
     * @var string
     *
     * @ORM\Column(name="leyenda", type="string", length=255)
     */
    private $leyenda;

    /**
     * @var integer
     * @ORM\OneToMany(targetEntity="BackBundle\Entity\Asiento", mappedBy="comprobante", cascade={"persist", "remove"})
     */
    protected $asientos;

    /**
     * Constructor
     */
    public function __construct() {
        $this->asientos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->estado = 'Generado';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Comprobante
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Comprobante
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return Comprobante
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Comprobante
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set concepto
     *
     * @param string $concepto
     *
     * @return Comprobante
     */
    public function setConcepto($concepto)
    {
        $this->concepto = $concepto;

        return $this;
    }

    /**
     * Get concepto
     *
     * @return string
     */
    public function getConcepto()
    {
        return $this->concepto;
    }

    /**
     * Set leyenda
     *
     * @param string $leyenda
     *
     * @return Comprobante
     */
    public function setLeyenda($leyenda)
    {
        $this->leyenda = $leyenda;

        return $this;
    }

    /**
     * Get leyenda
     *
     * @return string
     */
    public function getLeyenda()
    {
        return $this->leyenda;
    }

    /**
     * Add asiento
     *
     * @param \BackBundle\Entity\Asiento $asiento
     *
     * @return Comprobante
     */
    public function addAsiento(\BackBundle\Entity\Asiento $asiento)
    {
        $this->asientos[] = $asiento;

        return $this;
    }

    /**
     * Remove asiento
     *
     * @param \BackBundle\Entity\Asiento $asiento
     */
    public function removeAsiento(\BackBundle\Entity\Asiento $asiento)
    {
        $this->asientos->removeElement($asiento);
    }

    /**
     * Get asientos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAsientos()
    {
        return $this->asientos;
    }
}
