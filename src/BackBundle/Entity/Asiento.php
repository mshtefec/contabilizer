<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Asiento
 *
 * @ORM\Table(name="asiento")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\AsientoRepository")
 */
class Asiento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer")
     */
    private $numero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var float
     *
     * @ORM\Column(name="debe", type="float")
     */
    private $debe;

    /**
     * @var float
     *
     * @ORM\Column(name="haber", type="float")
     */
    private $haber;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=255)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="concepto", type="string", length=255)
     */
    private $concepto;

    /**
     * @var string
     *
     * @ORM\Column(name="leyenda", type="string", length=255)
     */
    private $leyenda;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="BackBundle\Entity\Comprobante", inversedBy="asientos")
     * @ORM\JoinColumn(name="comprobante_id", referencedColumnName="id", nullable=true)
     */
    private $comprobante;

    public function __construct()
    {
        $this->setDebe(0);
        $this->setHaber(0);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Asiento
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Asiento
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set debe
     *
     * @param float $debe
     *
     * @return Asiento
     */
    public function setDebe($debe)
    {
        $this->debe = $debe;

        return $this;
    }

    /**
     * Get debe
     *
     * @return float
     */
    public function getDebe()
    {
        return $this->debe;
    }

    /**
     * Set haber
     *
     * @param float $haber
     *
     * @return Asiento
     */
    public function setHaber($haber)
    {
        $this->haber = $haber;

        return $this;
    }

    /**
     * Get haber
     *
     * @return float
     */
    public function getHaber()
    {
        return $this->haber;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Asiento
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set concepto
     *
     * @param string $concepto
     *
     * @return Asiento
     */
    public function setConcepto($concepto)
    {
        $this->concepto = $concepto;

        return $this;
    }

    /**
     * Get concepto
     *
     * @return string
     */
    public function getConcepto()
    {
        return $this->concepto;
    }

    /**
     * Set leyenda
     *
     * @param string $leyenda
     *
     * @return Asiento
     */
    public function setLeyenda($leyenda)
    {
        $this->leyenda = $leyenda;

        return $this;
    }

    /**
     * Get leyenda
     *
     * @return string
     */
    public function getLeyenda()
    {
        return $this->leyenda;
    }

    /**
     * Set comprobante
     *
     * @param \BackBundle\Entity\Comprobante $comprobante
     *
     * @return Asiento
     */
    public function setComprobante(\BackBundle\Entity\Comprobante $comprobante = null)
    {
        $this->comprobante = $comprobante;

        return $this;
    }

    /**
     * Get comprobante
     *
     * @return \BackBundle\Entity\Comprobante
     */
    public function getComprobante()
    {
        return $this->comprobante;
    }
}
