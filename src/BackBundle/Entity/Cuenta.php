<?php

namespace BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cuenta
 *
 * @ORM\Table(name="cuenta")
 * @ORM\Entity(repositoryClass="BackBundle\Repository\AsientoRepository")
 */
class Cuenta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer")
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="imputable", type="string", length=255)
     */
    private $imputable;

    /**
     * @ORM\OneToMany(targetEntity="Cuenta", mappedBy="padre")
     * */
    private $hijas;

    /**
     * @ORM\ManyToOne(targetEntity="Cuenta", inversedBy="hijas")
     * @ORM\JoinColumn(name="padre_id", referencedColumnName="id")
     * */
    private $padre;

    public function __toString()
    {
        return (string) $this->getNombre();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hijas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Cuenta
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Cuenta
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set imputable
     *
     * @param string $imputable
     *
     * @return Cuenta
     */
    public function setImputable($imputable)
    {
        $this->imputable = $imputable;

        return $this;
    }

    /**
     * Get imputable
     *
     * @return string
     */
    public function getImputable()
    {
        return $this->imputable;
    }

    /**
     * Add hija
     *
     * @param \BackBundle\Entity\Cuenta $hija
     *
     * @return Cuenta
     */
    public function addHija(\BackBundle\Entity\Cuenta $hija)
    {
        $this->hijas[] = $hija;

        return $this;
    }

    /**
     * Remove hija
     *
     * @param \BackBundle\Entity\Cuenta $hija
     */
    public function removeHija(\BackBundle\Entity\Cuenta $hija)
    {
        $this->hijas->removeElement($hija);
    }

    /**
     * Get hijas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHijas()
    {
        return $this->hijas;
    }

    /**
     * Set padre
     *
     * @param \BackBundle\Entity\Cuenta $padre
     *
     * @return Cuenta
     */
    public function setPadre(\BackBundle\Entity\Cuenta $padre = null)
    {
        $this->padre = $padre;

        return $this;
    }

    /**
     * Get padre
     *
     * @return \BackBundle\Entity\Cuenta
     */
    public function getPadre()
    {
        return $this->padre;
    }
}
