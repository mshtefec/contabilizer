<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Asiento;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Asiento controller.
 *
 * @Route("admin/asiento")
 */
class AsientoController extends Controller
{
    /**
     * Lists all asiento entities.
     *
     * @Route("/", name="admin_asiento_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $asientos = $em->getRepository('BackBundle:Asiento')->findAll();

        return $this->render('asiento/index.html.twig', array(
            'asientos' => $asientos,
        ));
    }

    /**
     * Creates a new asiento entity.
     *
     * @Route("/new", name="admin_asiento_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $asiento = new Asiento();
        $form = $this->createForm('BackBundle\Form\AsientoType', $asiento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($asiento);
            $em->flush();

            return $this->redirectToRoute('admin_asiento_show', array('id' => $asiento->getId()));
        }

        return $this->render('asiento/new.html.twig', array(
            'asiento' => $asiento,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a asiento entity.
     *
     * @Route("/{id}", name="admin_asiento_show")
     * @Method("GET")
     */
    public function showAction(Asiento $asiento)
    {
        $deleteForm = $this->createDeleteForm($asiento);

        return $this->render('asiento/show.html.twig', array(
            'asiento' => $asiento,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing asiento entity.
     *
     * @Route("/{id}/edit", name="admin_asiento_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Asiento $asiento)
    {
        $deleteForm = $this->createDeleteForm($asiento);
        $editForm = $this->createForm('BackBundle\Form\AsientoType', $asiento);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_asiento_edit', array('id' => $asiento->getId()));
        }

        return $this->render('asiento/edit.html.twig', array(
            'asiento' => $asiento,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a asiento entity.
     *
     * @Route("/{id}", name="admin_asiento_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Asiento $asiento)
    {
        $form = $this->createDeleteForm($asiento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($asiento);
            $em->flush();
        }

        return $this->redirectToRoute('admin_asiento_index');
    }

    /**
     * Creates a form to delete a asiento entity.
     *
     * @param Asiento $asiento The asiento entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Asiento $asiento)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_asiento_delete', array('id' => $asiento->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
