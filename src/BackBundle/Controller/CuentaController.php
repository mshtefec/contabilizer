<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Cuenta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Cuenta controller.
 *
 * @Route("admin/cuenta")
 */
class CuentaController extends Controller
{
    /**
     * Lists all cuenta entities.
     *
     * @Route("/", name="admin_cuenta_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $cuentas = $em->getRepository('BackBundle:Cuenta')->findAll();

        return $this->render('cuenta/index.html.twig', array(
            'cuentas' => $cuentas,
        ));
    }

    /**
     * Creates a new cuenta entity.
     *
     * @Route("/new", name="admin_cuenta_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $cuenta = new Cuenta();
        $form = $this->createForm('BackBundle\Form\CuentaType', $cuenta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cuenta);
            $em->flush();

            return $this->redirectToRoute('admin_cuenta_show', array('id' => $cuenta->getId()));
        }

        return $this->render('cuenta/new.html.twig', array(
            'cuenta' => $cuenta,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a cuenta entity.
     *
     * @Route("/{id}", name="admin_cuenta_show")
     * @Method("GET")
     */
    public function showAction(Cuenta $cuenta)
    {
        $deleteForm = $this->createDeleteForm($cuenta);

        return $this->render('cuenta/show.html.twig', array(
            'cuenta' => $cuenta,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing cuenta entity.
     *
     * @Route("/{id}/edit", name="admin_cuenta_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Cuenta $cuenta)
    {
        $deleteForm = $this->createDeleteForm($cuenta);
        $editForm = $this->createForm('BackBundle\Form\CuentaType', $cuenta);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_cuenta_edit', array('id' => $cuenta->getId()));
        }

        return $this->render('cuenta/edit.html.twig', array(
            'cuenta' => $cuenta,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a cuenta entity.
     *
     * @Route("/{id}", name="admin_cuenta_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Cuenta $cuenta)
    {
        $form = $this->createDeleteForm($cuenta);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cuenta);
            $em->flush();
        }

        return $this->redirectToRoute('admin_cuenta_index');
    }

    /**
     * Creates a form to delete a cuenta entity.
     *
     * @param Cuenta $cuenta The cuenta entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cuenta $cuenta)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_cuenta_delete', array('id' => $cuenta->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
