<?php

namespace BackBundle\Controller;

use BackBundle\Entity\Comprobante;
use BackBundle\Entity\Asiento;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Comprobante controller.
 *
 * @Route("admin/comprobante")
 * @Template()
 */
class ComprobanteController extends Controller
{
    /**
     * Lists all comprobante entities.
     *
     * @Route("/", name="comprobante_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $comprobantes = $em->getRepository('BackBundle:Comprobante')->findAll();

        return array(
            'comprobantes' => $comprobantes,
        );
    }

    /**
     * Creates a new comprobante entity.
     *
     * @Route("/new", name="comprobante_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $comprobante = new Comprobante();
        $asientoD = new Asiento();
        $asientoH = new Asiento();
        $comprobante->addAsiento($asientoD);
        $comprobante->addAsiento($asientoH);
        $form = $this->createForm('BackBundle\Form\ComprobanteType', $comprobante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comprobante);
            $em->flush();

            return $this->redirectToRoute('comprobante_show', array('id' => $comprobante->getId()));
        }

        return array(
            'comprobante' => $comprobante,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a comprobante entity.
     *
     * @Route("/{id}", name="comprobante_show")
     * @Method("GET")
     */
    public function showAction(Comprobante $comprobante)
    {
        $deleteForm = $this->createDeleteForm($comprobante);

        return array(
            'comprobante' => $comprobante,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing comprobante entity.
     *
     * @Route("/{id}/edit", name="comprobante_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Comprobante $comprobante)
    {
        $deleteForm = $this->createDeleteForm($comprobante);
        $editForm = $this->createForm('BackBundle\Form\ComprobanteType', $comprobante);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comprobante_edit', array('id' => $comprobante->getId()));
        }

        return array(
            'comprobante' => $comprobante,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a comprobante entity.
     *
     * @Route("/{id}", name="comprobante_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Comprobante $comprobante)
    {
        $form = $this->createDeleteForm($comprobante);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comprobante);
            $em->flush();
        }

        return $this->redirectToRoute('comprobante_index');
    }

    /**
     * Creates a form to delete a comprobante entity.
     *
     * @param Comprobante $comprobante The comprobante entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Comprobante $comprobante)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('comprobante_delete', array('id' => $comprobante->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
