<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use SC\DatetimepickerBundle\Form\Type\DatetimeType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use BackBundle\Form\AsientoType;

class ComprobanteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero', null, array(
                'attr' => array('class' => 'form-control'),
            ))
            //->add('fecha')
            //->add('total')
            ->add('estado', null, array(
                'attr' => array('class' => 'form-control'),
            ))
            ->add('concepto', null, array(
                'attr' => array('class' => 'form-control'),
            ))
            ->add('leyenda', null, array(
                'attr' => array('class' => 'form-control'),
            ))
            // ->add('asientos', CollectionType::class, array(
            //     'entry_type'   => AsientoType::class,
            //     'prototype' => true,
            //     'allow_add' => true,
            // ))
            ->add('asientos', CollectionType::class, array(
                'entry_type' => AsientoType::class,
                'entry_options'  => array(
                    'attr' => array('class' => 'col-lg-8')
                ),
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Comprobante'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_comprobante';
    }


}
