<?php

namespace BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AsientoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero', null, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            //->add('fecha')
            ->add('debe', null, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->add('haber', null, array(
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            //->add('estado')
            //->add('concepto')
            //->add('leyenda')
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackBundle\Entity\Asiento'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backbundle_asiento';
    }


}
